#!/bin/bash
set -euo pipefail

if [ "${NODE_ENV}" = "development" ]; then
  ./node_modules/.bin/nodemon --inspect=0.0.0.0:9229 server.js
else
  node server.js
fi

exec "$@"