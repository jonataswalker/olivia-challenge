import setEnvironment from './set-environment.js';

setEnvironment();

export default {
  server: {
    host: '0.0.0.0',
    port: 3000,
    loggerLevel: process.env.NODE_ENV === 'development' ? 'info' : 'warn',
  },
};
