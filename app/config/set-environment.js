import { readFileSync } from 'fs';
import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';

import yaml from 'js-yaml';

import { decrypt } from '../utils/crypto.js';

const filename = fileURLToPath(import.meta.url);
const resolvePath = (file) => resolve(dirname(filename), file);

export default () => {
  const nodeEnvironment = process.env.NODE_ENV;
  if (
    !nodeEnvironment ||
    ['production', 'staging', 'development'].includes(nodeEnvironment) === false
  ) {
    throw new Error('Unknown NODE_ENV configured!');
  }

  try {
    const document = yaml.load(readFileSync(resolvePath('../../env.yaml')));

    const variables = document[nodeEnvironment];

    Object.keys(variables).forEach((variable) => {
      let { value, encrypted } = variables[variable];

      if (encrypted) {
        value = decrypt(value);
      }

      process.env[variable] = value;
    });
  } catch (error) {
    throw new Error(error);
  }
};
