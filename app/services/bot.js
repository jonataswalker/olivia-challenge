import puppeteer from 'puppeteer';

export async function getAlphabetStock() {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setViewport({ width: 1280, height: 800 });
    await page.goto('https://fit-web-scraping-challenge.herokuapp.com/login');

    await page.waitForSelector('input[name="username"]');
    await page.type('input[name="username"]', 'oliver');
    await page.waitForSelector('input[name="password"]');
    await page.type('input[name="password"]', 'olivia');
    await page.keyboard.press('Enter');

    // await browser.waitForTarget(
    //   (target) =>
    //     target.url() === 'https://fit-web-scraping-challenge.herokuapp.com'
    // );

    await page.waitForNavigation();
    console.log('New Page URL:', page.url());

    const alphabetLink = await page.$('a[href="/alphabet"]');
    await alphabetLink.click();
    await page.waitForNavigation();

    console.log('URL:', page.url());

    await browser.close();
  } catch (error) {
    console.trace(error);
  }
}
