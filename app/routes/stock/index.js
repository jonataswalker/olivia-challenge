export default (server, options, next) => {
  server.route({
    method: 'GET',
    url: '/stock',
    handler: (request, reply) => {
      reply.send({ root: true });
    },
  });

  next();
};
