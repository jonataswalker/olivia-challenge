export default (server, options, next) => {
  server.route({
    method: 'GET',
    url: '/',
    handler: (request, reply) => {
      reply.send({ root: true });
    },
  });

  server.route({
    method: 'GET',
    url: '/health',
    handler: (request, reply) => {
      reply.send('healthy');
    },
  });

  next();
};
