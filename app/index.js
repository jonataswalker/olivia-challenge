import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';
import { readFileSync } from 'fs';

import fastify from 'fastify';
import Swagger from 'fastify-swagger';
import AutoLoad from 'fastify-autoload';

import config from './config/index.js';

const filename = fileURLToPath(import.meta.url);
const resolvePath = (file) => resolve(dirname(filename), file);
const pkg = JSON.parse(readFileSync(resolvePath('../package.json')));

export default (options = {}) => {
  const { port, host } = config.server;
  const app = fastify(options);

  app.register(Swagger, {
    routePrefix: '/documentation',
    exposeRoute: true,
    openapi: {
      info: {
        title: pkg.description,
        description: 'API Overview',
        version: pkg.version,
      },
      host: `${host}:${port}`,
      tags: [{ name: 'User', description: 'User related end-points' }],
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
  });

  app.register(AutoLoad, {
    dir: resolvePath('./routes'),
    dirNameRoutePrefix: false,
    options: { prefix: '/api', ...options },
  });

  return app;
};
