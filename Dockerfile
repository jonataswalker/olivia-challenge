FROM node:14-slim as builder

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /home
COPY package*.json ./

RUN npm ci


FROM node:14-slim

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

ARG PORT=3000
ENV PORT $PORT
EXPOSE $PORT 9229 9230

RUN apt-get update && apt-get install -y curl htop

USER node
WORKDIR /www

COPY docker-entrypoint.sh /usr/local/bin/
COPY --chown=node:node --from=builder /home/node_modules node_modules
COPY --chown=node:node . .

HEALTHCHECK --interval=30s --timeout=3s \
  CMD curl -f http://localhost:${PORT}/health || exit 1

ENTRYPOINT ["docker-entrypoint.sh"]
