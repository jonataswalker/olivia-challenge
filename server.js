import config from './app/config/index.js';
import app from './app/index.js';

const { port, host, loggerLevel } = config.server;

const server = app({
  logger: { level: loggerLevel, prettyPrint: true },
});

server
  .listen({ port, host })
  .then((address) => server.log.info(`Server listening on ${address}`))
  .catch((error) => {
    console.trace(error);
    throw new Error(error);
  });
